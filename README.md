# MyBar

This app is intended to be a mixology compendium, mixed with a simple, yet user friendly, home bar inventory system that allows you to see mixes you can do with your booze.

The app comes with the public [compendium](https://gitlab.com/my-bar/compendium) as a base book, but you can remove it or add other compendiums that follow the same file format
